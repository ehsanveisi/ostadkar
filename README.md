## Ostadkar Interview App

#### CRUD on a User model with basic authentication with JWT and authorization.


### Running
To set up the development server run the following commands:
 
  ```bash prepare_env.sh```

 ```docker-compose -f local.yml up```

It'll automatically set up .env, PostgreSQL and a Flask development server available at 0.0.0.0:8000.

You can obtain a token at "/obtain_token/" endpoint and interact with the user api at "/users".

### Tests
Tests are available at userapp.tests. You can execute them using the following command for example:

``` docker-compose -f local.yml run --rm flask python -m userapp.tests```

