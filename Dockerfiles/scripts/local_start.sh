#!/bin/sh

set -o errexit
set -o nounset

FLASK_APP=app flask run --host=0.0.0.0 --port 8000
