#!/bin/bash

mkdir -p .envs
echo "SECRET_KEY=thisissecretkey" > .envs/.flask
echo "POSTGRES_HOST=postgres" > .envs/.postgres
echo "POSTGRES_PORT=5432" >> .envs/.postgres
echo "POSTGRES_DB=dev_db" >> .envs/.postgres
echo "POSTGRES_TEST_DB=template1" >> .envs/.postgres
echo "POSTGRES_USER=dev_user" >> .envs/.postgres
echo "POSTGRES_PASSWORD=dev_password" >> .envs/.postgres
