import unittest
import json

from . import config

from . import app, db
from .models import User


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        app.config.from_object(config.TestingConfig)
        self.client = app.test_client()

        db.init_app(app)
        with app.app_context():
            db.create_all()
            user = User(username="test", password="test")
            db.session.add(user)
            db.session.commit()
            self.user_id = user.id

    def tearDown(self):
        with app.app_context():
            db.session.remove()
            db.drop_all()


class TestAPI(BaseTestCase):
    def _obtain_token(self, username, password):
        data = {"username": username, "password": password}
        return self.client.post(
            "/obtain_token/", data=json.dumps(data), content_type="application/json"
        )

    def test_obtain_token(self):
        res = self._obtain_token("testwront", "test")
        self.assertEqual(res.status_code, 401)

        res = self._obtain_token("test", "testwrong")
        self.assertEqual(res.status_code, 401)

        res = self._obtain_token("test", "test")
        self.assertEqual(res.status_code, 200)
        self.assertIn("access_token", res.json)
        self.assertIn("refresh_token", res.json)

    def test_authorized_user_CRUD(self):
        obtain_token_res = self._obtain_token("test", "test")
        headers = {"Authorization": "Bearer %s" % obtain_token_res.json["access_token"]}

        post_data = {"username": "new_user", "password": "password"}
        create_user_res = self.client.post(
            "/users/",
            headers=headers,
            data=json.dumps(post_data),
            content_type="application/json",
        )
        self.assertEqual(create_user_res.status_code, 201)
        new_user_id = create_user_res.json["id"]

        get_all_res = self.client.get("/users/", headers=headers)
        self.assertEqual(get_all_res.status_code, 200)
        self.assertEqual(type(get_all_res.json), list)

        get_single_res = self.client.get("/users/%s/" % new_user_id, headers=headers)
        self.assertEqual(get_single_res.status_code, 200)
        self.assertEqual(type(get_single_res.json), dict)

        put_data = {"username": "new_user_changed"}
        update_user_res = self.client.put(
            "/users/%s/" % new_user_id,
            headers=headers,
            data=json.dumps(put_data),
            content_type="application/json",
        )
        self.assertEqual(update_user_res.status_code, 204)

        delete_user_res = self.client.delete(
            "/users/%s/" % new_user_id, headers=headers, content_type="application/json"
        )
        self.assertEqual(delete_user_res.status_code, 204)

    def test_unauthorized_user_CRUD(self):
        post_data = {"username": "some_user", "password": "password"}
        create_user_res = self.client.post(
            "/users/", data=json.dumps(post_data), content_type="application/json"
        )
        self.assertEqual(create_user_res.status_code, 201)

        get_all_res = self.client.get("/users/")
        self.assertEqual(get_all_res.status_code, 401)

        get_single_res = self.client.get("/users/%s/" % self.user_id)
        self.assertEqual(get_single_res.status_code, 401)

        put_data = {"username": "new_user_changed"}
        update_user_res = self.client.put(
            "/users/%s/" % self.user_id,
            data=json.dumps(put_data),
            content_type="application/json",
        )
        self.assertEqual(update_user_res.status_code, 401)

        delete_user_res = self.client.delete(
            "/users/%s/" % self.user_id, content_type="application/json"
        )
        self.assertEqual(delete_user_res.status_code, 401)


if __name__ == "__main__":
    unittest.main()
