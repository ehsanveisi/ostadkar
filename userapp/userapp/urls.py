from . import api
from .views import UserView, UserListCreateView, ObtainToken

api.add_resource(UserView, "/users/<user_id>/")
api.add_resource(UserListCreateView, "/users/")
api.add_resource(ObtainToken, "/obtain_token/")
