from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    fresh_jwt_required,
)
from flask_restful import Resource, reqparse
from flask import request

from .serializers import UserSchema
from .models import User
from . import db


class ObtainToken(Resource):
    """Resource with POST method to obtain JTW tokens."""

    # defining the request parser and expected arguments in the request
    parser = reqparse.RequestParser()
    parser.add_argument(
        "username",
        type=str,
        required=True,
        help="This field cannot be blank.",
        location="json",
    )
    parser.add_argument(
        "password",
        type=str,
        required=True,
        help="This field cannot be blank.",
        location="json",
    )

    def post(self):
        """POST method to obtain JWT token.
        :arg:
            username
            password

        :return:
                {
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }
        """
        data = self.parser.parse_args()
        # read from database to find the user and then check the password
        user = User.query.filter_by(username=data["username"], is_deleted=False).first()

        if not (user and user.check_password(data["password"])):
            return {"message": "Invalid Credentials!"}, 401

        # when authenticated, return a fresh access token and a refresh token
        access_token = create_access_token(identity=user.id, fresh=True)
        refresh_token = create_refresh_token(user.id)
        return {"access_token": access_token, "refresh_token": refresh_token}, 200


class UserView(Resource):
    """User resource handling single GET, PUT and DELETE methods."""

    def get_object(self, user_id):
        return User.query.filter_by(id=user_id, is_deleted=False).first()

    @fresh_jwt_required
    def get(self, user_id):
        user = self.get_object(user_id)
        if not user:
            return {"message": "Not Found!"}, 404

        return UserSchema().dump(user).data

    @fresh_jwt_required
    def delete(self, user_id):
        user = self.get_object(user_id)
        if user:
            user.delete()
        return "", 204

    @fresh_jwt_required
    def put(self, user_id):
        data = request.get_json()
        user = self.get_object(user_id)
        if not user:
            return {"message": "Not Found!"}, 404

        unmarsh_result = UserSchema().load(data, instance=user)
        user = unmarsh_result.data
        if not user:
            return unmarsh_result.errors, 400
        db.session.add(user)
        db.session.commit()
        return UserSchema().dump(user).data, 204


class UserListCreateView(Resource):
    """User resource handling list GET, POST methods."""

    @fresh_jwt_required
    def get(self):
        users = User.query.filter_by(is_deleted=False).all()
        return UserSchema().dump(users, many=True).data

    def post(self):
        data = request.get_json()
        unmarsh_result = UserSchema().load(data)
        user = unmarsh_result.data
        if not user:
            return unmarsh_result.errors, 400
        db.session.add(user)
        db.session.commit()
        return UserSchema().dump(user).data, 201
