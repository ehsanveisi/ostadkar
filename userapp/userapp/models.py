import re

from sqlalchemy.orm import validates

from . import db, bcrypt
import datetime


class User(db.Model):
    """User model"""
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), unique=True, nullable=False)
    password = db.Column(db.String(60))
    first_name = db.Column(db.String(60))
    last_name = db.Column(db.String(60))
    registration_datetime = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    phone_number = db.Column(db.String(11))
    is_deleted = db.Column(db.Boolean, default=False)

    @validates('phone_number')
    def validate_phone_number(self, key, phone_number):
        """simple validation for internal phone numbers"""
        assert re.match('^0\d{10}$', phone_number)
        return phone_number

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_password(kwargs["password"])

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password.encode('utf-8'), password)

    def delete(self):
        self.is_deleted = True

    def __repr__(self):
        return self.username
