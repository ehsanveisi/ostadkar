from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt

from . import config

app = Flask(__name__)
app.config.from_object(config.DevelopmentConfig)
db = SQLAlchemy(app)
ma = Marshmallow(app)  # must never get initialized before SQLAlchemy
api = Api(app)
bcrypt = Bcrypt(app)
jwt_manager = JWTManager(app)

from . import urls
